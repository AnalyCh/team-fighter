package com.example.analychinacalle.team_fighter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase

class ListarPuntaje : AppCompatActivity() {

    lateinit var listsRecyclerView: RecyclerView
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("Puntaje")



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_puntaje)

        listsRecyclerView = findViewById(R.id.RecyclerView)
        listsRecyclerView.layoutManager = LinearLayoutManager(this)
        listsRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref)
    }
}
