package com.example.analychinacalle.team_fighter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.support.annotation.IntegerRes
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity()
{

    internal  lateinit var gameScoreTextView: TextView
    internal lateinit var  timeleft: TextView
    internal lateinit var tapMeButton: Button
    internal lateinit var listarbtn: Button
    internal var score =0

    internal var gameStarted = false

    internal lateinit var countDownTimer: CountDownTimer
    internal var countDownInterval = 1000L
    internal var initialCountDown = 10000L //CAM
    internal var timeLeft = 10 //CAM

    internal var TAG = MainActivity::class.java.simpleName

    companion object {              //Similar a una clase dentro de una clase
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    //CADA VEZ QUE SE GIRA LA PANTALLA SE LLAMA A LA FUNCIÓN onCreate --> el score es nuevamente 0
    //La actividad en la que estamos intenta guardar el estado y se destruye y luego se recrea   -> Cada vez que se cambia la orientación ºº Se cambia el leng durante la app.


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "onCreate called. Score is $score")
        gameScoreTextView = findViewById(R.id.game_score_textView)
        timeleft = findViewById(R.id.time_left_textView)
        tapMeButton = findViewById(R.id.tap_me_button)
        listarbtn = findViewById(R.id.btnListar)


        if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            //restoreGame()
        }else {
            resetGame()
        }

        tapMeButton.setOnClickListener{_-> incrementScore()}
        listarbtn.setOnClickListener{_ -> mostrarLista()}


    }
    
    private fun mostrarLista(){
        val listaPuntajeIntent = Intent(this, ListarPuntaje::class.java)
        //listaPuntajeIntent.putExtra(INTENT_LIST)
        startActivity(listaPuntajeIntent)
    }


    private fun resetGame(){
        score = 0
        timeLeft = 10 //CAM
        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = gameScore

        val timeLetfText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeleft.text = timeLetfText
        //time_left_textView.text = timeLetfText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() /1000
                //time_left_textView.text= getString(R.string.time_left, Integer.toString(timeLeft))
                timeleft.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                showCreateListDialog()

//                endGame()
            }
        }
        gameStarted = false


    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame() {

        Toast.makeText(this, "Game over", Toast.LENGTH_LONG).show()

        //

        resetGame()

    }

    private fun incrementScore(){
        score ++

        //val newScore = "Your score " + Integer.toString(score)
        val newScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = newScore

        if(!gameStarted){
            startGame()
        }
    }


    private fun guardarBase(nombre: String){
        //habilitarNombre()
        //Toast.makeText(this, "entro a guardar", Toast.LENGTH_LONG)
        //val nombre = showCreateListDialog()
        //val nombre = name.text.toString().trim()
        val puntaje = Integer.toString(score)


        val myDatabase = FirebaseDatabase.getInstance().getReference("Puntaje")
        val idPuntaje = myDatabase.push().key.toString()
        val puntajeN = Puntaje(idPuntaje, puntaje, nombre)
        Log.d("tag", "mensaje es: " +nombre + puntaje)
        myDatabase.child(idPuntaje).setValue(puntajeN).addOnCompleteListener {
            Toast.makeText(this, "saved c:", Toast.LENGTH_LONG)

            //showListDetail(idPuntaje)
        }

    }

    private fun showCreateListDialog() {

        val dialogTitle = getString(R.string.your_name)
        val positiveButtonTitle = getString(R.string.save_player_name)


        val builder = AlertDialog.Builder(this)
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)

        var nombre= "aux"

        builder.setPositiveButton(positiveButtonTitle) { dialog, i ->

            if (listTitleEditText.text.isEmpty()){
                return@setPositiveButton
            }

            nombre = listTitleEditText.text.toString().trim()
            guardarBase(nombre)
            dialog.dismiss()
            endGame()
        }
        builder.create().show()



    }

    fun restoreGame(){
        val restoredScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restoredScore

        val restoredTime = getString(R.string.time_left, Integer.toString(timeLeft))
        timeleft.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                timeleft.text = getString(R.string.time_left, timeLeft.toString())

            }

            override fun onFinish() {
                endGame()
            }
        }


    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState!!.putInt(SCORE_KEY,score)
        outState!!.putInt(TIME_LEFT_KEY, timeLeft)
        countDownTimer.cancel()

        Log.d(TAG, "onSaveInstanceState: score = $score n timeleft = $timeleft")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "onDestroy called")
    }

}
