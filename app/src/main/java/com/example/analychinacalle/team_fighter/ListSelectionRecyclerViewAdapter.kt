package com.example.analychinacalle.team_fighter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.DatabaseReference

class ListSelectionRecyclerViewAdapter(ref: DatabaseReference):
RecyclerView.Adapter<ListSelectionRecyclerViewHolder>(){


    var puntajesList: MutableList<Puntaje> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListSelectionRecyclerViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_selection_view_holder, parent, false)

        return ListSelectionRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return puntajesList.count()
    }

    override fun onBindViewHolder(holder: ListSelectionRecyclerViewHolder, position: Int) {
        holder.puntaje.text = puntajesList[position].name + " "+ puntajesList[position].score
    }


}